import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Platform, View,Dimensions,Image,TouchableOpacity,StatusBar,ImageBackground,StyleSheet ,FlatList,TextInput} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Spinner,
  Body,
  Toast,
  Input,

} from "native-base";
import { Actions } from "react-native-router-flux";
import * as appStateSelector from "../../../reducers/driver/appState";
//import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import AsyncStorage from '@react-native-community/async-storage';
import pr from 'pr-unit';
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import { checkUser, userLoginRequest,getremaningtimer,setlistorder,setgenderchoice } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";
import RNPickerSelect from 'react-native-picker-select';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import _ from "lodash";
import { changePageStatus, currentLocationUser, signInUser } from '../../../actions/driver/home';
import { fetchUserCurrentLocationAsync, syncDataAsync, mapDeviceIdToUser,getpooldata } from '../../../actions/driver/home';
import OneSignal from "react-native-onesignal";
import config from "../../../../config";
import Modal from "react-native-modal";
import Contacts from 'react-native-contacts';

//import DeviceInfo from 'react-native-device-info';
import { emit } from "../../../services/socket";
let that = null;
const deviceWidth = Dimensions.get('window').width; 
const deviceHeight = Dimensions.get('window').height; 




function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  console.log('state.driver.appState',state.app);
  return {
   
  };
}

class Home extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    
    
  };
 timer = null;
  

  constructor(props) {
    super(props);
    

    this.state = {
     data:[{image:'',title:'Создать',second:'новый сайт',subtitle:'Начать'},{image:'',title:'Мой',second:'сайт 1',subtitle:'Перейти'},{image:'',title:'Мой',second:'сайт 2',subtitle:'Перейти'},{image:'',title:'Мой',second:'сайт 3',subtitle:'Перейти'},{image:'',title:'Мой',second:'сайт 4',subtitle:'Перейти'},{image:'',title:'Мой',second:'сайт 5',subtitle:'Перейти'}],
     data2:[{image:'',title:'Заглавие сайта',subtitle:'Уникальное торговое предложение с ключевыми преимуществами компании'},{image:'',title:'Описание предложения',subtitle:'Подробное описание вашего предложения  с ключевыми преимуществами'},{image:'',title:'Заглавие сайта',subtitle:'Уникальное торговое предложение с ключевыми преимуществами компании'},{image:'',title:'Заглавие сайта',subtitle:'Уникальное торговое предложение с ключевыми преимуществами компании'},{image:'',title:'Заглавие сайта',subtitle:'Уникальное торговое предложение с ключевыми преимуществами компании'},{image:'',title:'Заглавие сайта',subtitle:'Уникальное торговое предложение с ключевыми преимуществами компании'},]
    };
    
  }

 

  componentWillUnmount(){
   
  }
 
 renderItem = ({item, index}) => {
    
    return (
      <View style={{backgroundColor:'#ffffff',borderRadius:20,height:110,width:deviceWidth/5,marginLeft:15,}}>
      
     
                {index==0 &&  
                 <View style={{backgroundColor:'#6262D9',borderRadius:15,width:30,height:30,justifyContent:'center',alignItems:'center',margin:10}}>
                  <Image source={require("../../../../assets/images/Edit_Square.png")}  style={{ width:20, height: 20,resizeMode:'cover'}}/> 
                  </View>}
                {index==1 && <View style={{backgroundColor:'#F2973D',borderRadius:15,width:30,height:30,justifyContent:'center',alignItems:'center',margin:10}}>
                       <Image source={require("../../../../assets/images/Globe.png")}  style={{ width:20, height: 20,resizeMode:'cover',padding:10}}/> 
               </View> }
                {index==2 &&  
                  <View style={{backgroundColor:'#1DBF53',borderRadius:15,width:30,height:30,justifyContent:'center',alignItems:'center',margin:10}}>

                 <Image source={require("../../../../assets/images/Globe.png")}  style={{ width:20, height: 20,resizeMode:'cover'}}/> 
                 </View> }
                 {index==3 &&  
                   <View style={{backgroundColor:'#1DBF53',borderRadius:15,width:30,height:30,justifyContent:'center',alignItems:'center',margin:10}}>

                 <Image source={require("../../../../assets/images/Globe.png")}  style={{ width:20, height: 20,resizeMode:'cover',}}/> 
                 </View> }
                 {index==4 &&  
                  <View style={{backgroundColor:'#1DBF53',borderRadius:15,width:30,height:30,justifyContent:'center',alignItems:'center',margin:10}}>

                 <Image source={require("../../../../assets/images/Globe.png")}  style={{ width:20, height: 20,resizeMode:'cover'}}/> 
                 </View> }
                 {index==5 &&  
                  <View style={{backgroundColor:'#1DBF53',borderRadius:15,width:30,height:30,justifyContent:'center',alignItems:'center',margin:10}}>

                 <Image source={require("../../../../assets/images/Globe.png")}  style={{ width:20, height: 20,resizeMode:'cover'}}/> 
                 </View> }
          <View style={{marginTop:5,width:'80%',marginLeft:10,}}>
           <Text style={styles.textheading}>{item.title}</Text>
           <Text style={styles.textheading}>{item.second}</Text>  
           </View> 
            <Text style={styles.textheading2}>{item.subtitle}</Text>  
             
        
        

       </View>        
    )
  };
  renderItem2 = ({item, index}) => {
    
    return (
      <View style={{backgroundColor:'#ffffff',borderRadius:10,height:deviceHeight/4.5,marginHorizontal:'1%',elevation: 5,shadowColor: '#000',padding:'3%',marginTop:'2%'}}>
      <View style={{margin:'2%',flexDirection:'row',alignItems:'center',justifuContent:'center',padding:3}}>
                  <Image                      
                       source={require("../../../../assets/images/bar.png")}
                      style={{ width:20, height: 20,resizeMode:'cover'}}
                      />
                       <Text style={styles.textvalue}>{item.title}</Text> 
      </View>
          <View style={{width:'100%',padding:10,}}>
           <Text style={styles.textsub}>{item.subtitle}</Text>  
           </View> 
           <View style={{flexDirection:'row',marginHorizontal:'3%',}}>
             <View style={{backgroundColor:'#5B63F4',borderWidth:2,borderColor:'#5B63F4',width:'20%'}}></View>
              <View style={{backgroundColor:'#F7F7F8',borderWidth:2,borderColor:'#F7F7F8',width:'80%'}}></View>
           </View>
            <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:'2%',padding:'2%'}}>
                  <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',}}>
                     <Image                      
                       source={require("../../../../assets/images/time_aquare.png")}
                      style={{ width:12, height: 12,resizeMode:'stretch'}}
                      />
                      <Text style={{color:'#8FA4B2',fontSize:10,marginLeft:10}}>1 из 3</Text>
                  </View>
                  <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:'#8FA4B2',fontSize:10,marginRight:10}}>Начать</Text>
                     <Image                      
                       source={require("../../../../assets/images/vector2.png")}
                      style={{ width:7, height: 7,resizeMode:'stretch',}}
                      />
                      
                  </View>
            </View>
             
        
        

       </View>        
    )
  };
 
  render() {
   
    
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <StatusBar
            animated={true}
            backgroundColor='rgba(205, 178, 108, 0.1)'
            barStyle="dark-content"
            showHideTransition="none"
          />
      <ImageBackground source={ require('../../../../assets/images/Background.png')}  style={{width: deviceWidth, height: deviceHeight/3.8, resizeMode:'cover',borderBottomWidth:2,borderColor:'lightgray' ,}}> 
     
      
               <View style={styles.mainmenu}>
               <View style={{justifuContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:17,color:'black',fontWeight:'bold',}}>Главная</Text>
                </View>
                <TouchableOpacity style={styles.userback}>
                   <Image                      
                       source={require("../../../../assets/images/User.png")}
                      style={{ width:14, height: 7,padding:10}}
                      />
                  </TouchableOpacity>
                
                   
            </View>
            <View style={{marginHorizontal:10,marginTop:15,marginBottom:5}}>
              <FlatList
                  horizontal={true}
                   style={{ margin:3}}
                   data={this.state.data}
                   extraData={this.state}
                   renderItem={this.renderItem}
                   showsHorizontalScrollIndicator={false}

                   />
          
            </View>
            
            </ImageBackground>
            <ImageBackground source={ require('../../../../assets/images/background2.png')}  style={{width: deviceWidth, height: 'auto', resizeMode:'cover',borderBottomWidth:2,borderColor:'lightgray' ,flex:1}}> 
     
      
               <View style={styles.mainmenu}>
               
                
                   <Image                      
                       source={require("../../../../assets/images/Upload.png")}
                      style={{ width:14, height: 7,padding:10}}
                      />
                  
                <View style={{justifuContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <Text style={{fontSize:17,color:'black',}}>mysite1.qwik.ws</Text>
                  
                   <Image                      
                       source={require("../../../../assets/images/vector.png")}
                      style={{ width:13, height: 10,resizeMode:'stretch',marginLeft:5}}
                      />
                  
                </View>
               
               <Image                      
                   source={require("../../../../assets/images/Discovery.png")}
                  style={{ width:14, height: 7,padding:10}}
                  />
             
            </View>
            <View style={{margin:10}}>
                  <Text style={{fontSize:17,color:'black',fontWeight:'bold',}}>Главная страница</Text>
                </View>
            <View style={{flex:1,marginHorizontal:10,marginTop:10}}>
              <FlatList
                  
                   style={{ margin:3}}
                   data={this.state.data2}
                   extraData={this.state}
                   renderItem={this.renderItem2}
                   showsVerticalScrollIndicator={false}

                   />
          
            </View>
            
            </ImageBackground>
               <View style={styles.footer}>
                 <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:'10%'}}>
                  <TouchableOpacity>
                   <Image                      
                       source={require("../../../../assets/images/Home.png")}
                      style={{ width:14, height: 7,padding:15}}
                      />
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => Actions.Ads()}>
                      <Image                      
                       source={require("../../../../assets/images/Activity.png")}
                      style={{ width:14, height: 7,padding:15}}
                      />
                      </TouchableOpacity>
                      <TouchableOpacity>
                      <Image                      
                       source={require("../../../../assets/images/3user.png")}
                      style={{ width:14, height: 7,padding:15}}
                      />
                      </TouchableOpacity>
                      <TouchableOpacity>
                      <Image                      
                       source={require("../../../../assets/images/Chat.png")}
                      style={{ width:14, height: 7,padding:15}}
                      />
                      </TouchableOpacity>
                 </View>
               </View>
      </Container>
    );
  }
}



Home = reduxForm({
  
})(Home);

Home = connect(mapStateToProps)(Home);

export default Home;
