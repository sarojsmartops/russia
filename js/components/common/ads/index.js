import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Platform, View,Dimensions,Image,TouchableOpacity,StatusBar,ImageBackground,StyleSheet ,FlatList,TextInput} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Spinner,
  Body,
  Toast,
  Input,

} from "native-base";
import { Actions } from "react-native-router-flux";
import * as appStateSelector from "../../../reducers/driver/appState";
//import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import AsyncStorage from '@react-native-community/async-storage';
import pr from 'pr-unit';
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import { checkUser, userLoginRequest,getremaningtimer,setlistorder,setgenderchoice } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";
import RNPickerSelect from 'react-native-picker-select';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import _ from "lodash";
import { changePageStatus, currentLocationUser, signInUser } from '../../../actions/driver/home';
import { fetchUserCurrentLocationAsync, syncDataAsync, mapDeviceIdToUser,getpooldata } from '../../../actions/driver/home';
import OneSignal from "react-native-onesignal";
import config from "../../../../config";
import Modal from "react-native-modal";
import Contacts from 'react-native-contacts';

//import DeviceInfo from 'react-native-device-info';
import { emit } from "../../../services/socket";
let that = null;
const deviceWidth = Dimensions.get('window').width; 
const deviceHeight = Dimensions.get('window').height; 




function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  console.log('state.driver.appState',state.app);
  return {
   
  };
}

class Ads extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    
    
  };
 timer = null;
  

  constructor(props) {
    super(props);
    

    this.state = {
     
    };
    
  }

 

  componentWillUnmount(){
   
  }
 
 
  render() {
   
    
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <StatusBar
            animated={true}
            backgroundColor='#ffffff'
            barStyle="dark-content"
            showHideTransition="none"
          />
               <View style={styles.mainmenu}>
                <TouchableOpacity onPress={()=>Actions.pop()}>
                   <Image                      
                       source={require("../../../../assets/images/Stroke.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                <View style={{justifuContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:17,color:'black',fontWeigth:'bold'}}>Рекламная кампания</Text>
                </View>
                   
            </View>
            <View style={{flex:1}}>
       <View style={styles.text}>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'15%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'15%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'15%'}}></View> 
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'15%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'15%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'15%'}}></View>  
       </View>
       <View style={{marginHorizontal:'10%',marginTop:'10%'}}>
       <View style={{}}>
          <Text style={{fontSize:15,color:'black'}}>Название рекламной компании</Text>
       </View>

        
                        <TextInput
                          placeholder='Название'
                          style={{ borderColor:'#e6e6e6', borderBottomWidth:1,  color:'#000', width:deviceWidth-20, fontSize:13,width:'95%'}}
                          multiline
                          numberOfLines={1}
                          placeholderTextColor="#777"
                          editable={true}
                           onChange={(event) => this.setState({description: event.nativeEvent.text})}
                           value={this.state.description}
                          
                        />            
         </View> 
         <View style={{marginHorizontal:'10%',marginTop:'10%'}}>
       <View style={{}}>
          <Text style={{fontSize:15,color:'#172026',fontWeigth:'bold'}}>Добавить сторонний сайт или ссылку</Text>
          <Text style={{fontSize:13,color:'#172026'}}>Введите URL-адрес сайта</Text>
       </View>

        
                        <TextInput
                          placeholder='URL'
                          style={{ borderColor:'#e6e6e6', borderBottomWidth:1,  color:'#000', width:deviceWidth-20, fontSize:13,width:'95%'}}
                          multiline
                          numberOfLines={1}
                          placeholderTextColor="#777"
                          editable={true}
                           onChange={(event) => this.setState({description: event.nativeEvent.text})}
                           value={this.state.description}
                          
                        />            
         </View>  
         <View style={{justifuContent:'center',alignItems:'center',marginTop:'10%'}}>
           <Text style={{fontSize:12,color:'black'}}>Для более глубокой аналитики, рекомендуем </Text>
           <Text style={{fontSize:12,color:'blue'}}>создать сайт на платформе Qwik</Text>
         </View>  
         </View>
         <TouchableOpacity style={{marginHorizontal:'5%',borderRadius:20,backgroundColor:'#5B63F4',justifuContent:'center',alignItems:'center',padding:'3%',marginBottom:'5%'}} onPress={() => Actions.Ads2()}>
             <Text style={{color:'#fff'}}>Далее</Text>
         </TouchableOpacity>       
      </Container>
    );
  }
}



Ads = reduxForm({
  
})(Ads);

Ads = connect(mapStateToProps)(Ads);

export default Ads;
