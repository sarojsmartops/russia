import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Platform, View,Dimensions,Image,TouchableOpacity,StatusBar,ImageBackground,StyleSheet ,FlatList,TextInput} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Spinner,
  Body,
  Toast,
  Input,

} from "native-base";
import { Actions } from "react-native-router-flux";
import * as appStateSelector from "../../../reducers/driver/appState";
//import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import AsyncStorage from '@react-native-community/async-storage';
import pr from 'pr-unit';
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import { checkUser, userLoginRequest,getremaningtimer,setlistorder,setgenderchoice } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";
import RNPickerSelect from 'react-native-picker-select';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import _ from "lodash";
import { changePageStatus, currentLocationUser, signInUser } from '../../../actions/driver/home';
import { fetchUserCurrentLocationAsync, syncDataAsync, mapDeviceIdToUser,getpooldata } from '../../../actions/driver/home';
import OneSignal from "react-native-onesignal";
import config from "../../../../config";
import Modal from "react-native-modal";
import Contacts from 'react-native-contacts';

//import DeviceInfo from 'react-native-device-info';
import { emit } from "../../../services/socket";
let that = null;
const deviceWidth = Dimensions.get('window').width; 
const deviceHeight = Dimensions.get('window').height; 




function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  console.log('state.driver.appState',state.app);
  return {
   
  };
}

class New3 extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    
    
  };
 timer = null;
  

  constructor(props) {
    super(props);
    

    this.state = {
     
    };
    
  }

 

  componentWillUnmount(){
   
  }
 
 
  render() {
   
    
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <StatusBar
            animated={true}
            backgroundColor='rgba(205, 178, 108, 0.1)'
            barStyle="dark-content"
            showHideTransition="none"
          />
               <View style={styles.mainmenu2}>
                <TouchableOpacity onPress={()=>Actions.pop()}>
                   <Image                      
                       source={require("../../../../assets/images/Stroke.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                <View style={{justifuContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:17,color:'black'}}>Формат</Text>
                </View>
                <TouchableOpacity>
                   <Image                      
                       source={require("../../../../assets/images/XCircle.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                   
            </View>
            <View style={{flex:1}}>
               <View style={styles.text}>
                  <View style={{backgroundColor:'#5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
                  <View style={{backgroundColor:'#5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
                  <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
                  <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
                  <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
                  <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
                  <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
                  <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
                  <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>  
               </View>
       
         <View style={{marginHorizontal:'10%',marginTop:'10%'}}>
      
          <Text style={{fontSize:15,color:'#172026',fontWeight:'bold'}}>ВВы добавили изображение</Text>
          <Text style={{fontSize:13,color:'#172026',width:'60%'}}>Теперь эта фотография будет вашей обложкой. Вы можете ее заменить </Text>
          </View> 
          <View style={{marginHorizontal:'10%',marginTop:'5%'}}>
         <Image                      
               source={require("../../../../assets/images/image_1674.png")}
               style={{width:deviceWidth-100, height: deviceHeight/4,resizeMode:'stretch'}}
              /> 
         </View>
         </View>

         <TouchableOpacity style={{marginHorizontal:'5%',borderRadius:20,backgroundColor:'#5B63F4',justifuContent:'center',alignItems:'center',padding:'3%',marginBottom:'5%'}} onPress={()=>Actions.New4()}>
             <Text style={{color:'#fff'}}>Далее</Text>
         </TouchableOpacity>       
      </Container>
    );
  }
}



New3 = reduxForm({
  
})(New3);

New3 = connect(mapStateToProps)(New3);

export default New3;
