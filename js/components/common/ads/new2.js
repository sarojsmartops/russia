import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Platform, View,Dimensions,Image,TouchableOpacity,StatusBar,ImageBackground,StyleSheet ,FlatList,TextInput,ScrollView} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Spinner,
  Body,
  Toast,
  Input,

} from "native-base";
import { Actions } from "react-native-router-flux";
import * as appStateSelector from "../../../reducers/driver/appState";
//import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import AsyncStorage from '@react-native-community/async-storage';
import pr from 'pr-unit';
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import { checkUser, userLoginRequest,getremaningtimer,setlistorder,setgenderchoice } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";
import RNPickerSelect from 'react-native-picker-select';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import _ from "lodash";
import { changePageStatus, currentLocationUser, signInUser } from '../../../actions/driver/home';
import { fetchUserCurrentLocationAsync, syncDataAsync, mapDeviceIdToUser,getpooldata } from '../../../actions/driver/home';
import OneSignal from "react-native-onesignal";
import config from "../../../../config";
import Modal from "react-native-modal";
import Contacts from 'react-native-contacts';

//import DeviceInfo from 'react-native-device-info';
import { emit } from "../../../services/socket";
let that = null;
const deviceWidth = Dimensions.get('window').width; 
const deviceHeight = Dimensions.get('window').height; 




function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  console.log('state.driver.appState',state.app);
  return {
   
  };
}

class New2 extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    
    
  };
 timer = null;
  

  constructor(props) {
    super(props);
    

    this.state = {
     
    };
    
  }

 

  componentWillUnmount(){
   
  }
  

  renderItem = ({item, index}) => {
    
    return (
      <View style={{backgroundColor:'#ffffff',borderRadius:20,height:110,width:deviceWidth/5,marginLeft:15,}}>
      
     

       </View>        
    )
  };
 
  render() {
   
    
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <StatusBar
            animated={true}
            backgroundColor='rgba(205, 178, 108, 0.1)'
            barStyle="dark-content"
            showHideTransition="none"
          />
          <ScrollView>
               <View style={styles.mainmenu2}>
                <TouchableOpacity onPress={()=>Actions.pop()}>
                   <Image                      
                       source={require("../../../../assets/images/Stroke.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                <View style={{justifuContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:17,color:'black'}}>Формат</Text>
                </View>
                <TouchableOpacity>
                   <Image                      
                       source={require("../../../../assets/images/XCircle.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                   
            </View>
            <View style={{flex:1}}>
       <View style={styles.text}>
          <View style={{backgroundColor:'#5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
          <View style={{backgroundColor:'#5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>  
       </View>
       <View style={{marginHorizontal:'5%',marginTop:'10%',borderWidth:1,borderColor:'#e6e6e6',padding:'4%',borderRadius:10}}>
       <View style={{marginTop:5}}>
          <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Добавить готовый рекламный пост</Text>
          <Text style={{fontSize:15,color:'#8FA4B2'}}>Выбери и загрузи файл с устройства 1080х1080 p</Text>
       </View>
       <View style={{
          paddingLeft:10,
          height:100,
          marginBottom:10,
          borderStyle: 'dashed',
          borderRadius: 1,
          borderWidth: 1,
          borderColor: '#5B63F4',
          borderTopColor:'white',marginTop:'2%'
         }} >
             <View style={{justifuContent:'center',alignItems:'center',marginTop:'10%'}}>
                      <Image                      
                       source={require("../../../../assets/images/Import.png")}
                       style={{ width:24, height: 24,padding:10,resizeMode:'contain'}}
                      />
                 <Text style={{fontSize:13,color:'#5B63F4'}}>Добавь изображения высокого разрешения</Text>

            </View>
         </View>

       
          </View> 
        <View style={{marginHorizontal:'5%',marginTop:'10%',borderWidth:1,borderColor:'#e6e6e6',padding:'4%',borderRadius:10}}>

         <View style={{marginTop:5}}>
          <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Или воспользуйтесь редактором</Text>
          <Text style={{fontSize:15,color:'#8FA4B2'}}>Выбери один из предложенных стандартных вариантов </Text>
           <View style={{flexDirection:'row',justifyContent:'space-between',}}>
             <Text>#популярное </Text>
             <View style={{flexDirection:'row'}}>
                  <Text style={{color:'#2F80ED'}}>Поиск </Text>
                  <Image                      
                       source={require("../../../../assets/images/MagnifyingGlass.png")}
                       style={{ width:20, height: 20,padding:10,resizeMode:'contain'}}
                      />
             </View>
             
           </View>
           <ScrollView horizontal={true} style={{marginLeft:3}}>

             <View style={{
          paddingLeft:10,
          height:deviceWidth/4,width:deviceWidth/2.7,
          marginBottom:10,
          borderStyle: 'dashed',
          borderRadius: 1,
          borderWidth: 1,
          borderColor: '#5B63F4',
          borderTopColor:'white',marginTop:'2%'
         }} >
             <View style={{justifuContent:'center',alignItems:'center',marginTop:'10%'}}>
                      <Image                      
                       source={require("../../../../assets/images/Import.png")}
                       style={{ width:24, height: 24,padding:10,resizeMode:'contain'}}
                      />
                 <Text style={{fontSize:13,color:'#5B63F4'}}>Добавь изображения высокого разрешения</Text>

            </View>
         </View>
         <View style={{
          paddingLeft:10,
          height:deviceWidth/4,width:deviceWidth/2.5,
          marginBottom:10,
          
          borderRadius: 2,
          marginTop:'2%'
         }}>
             


                      <ImageBackground                     
                       source={require("../../../../assets/images/image_1692.png")}
                       style={{ height:deviceWidth/4,width:deviceWidth/2.5,resizeMode:'contain'}}
                      >

                       <View style={{position:'absolute',top:5,left:5}}>
                         <Image                      
                           source={require("../../../../assets/images/Frame_137.png")}
                           style={{ width:20, height: 20,padding:10,resizeMode:'contain'}}
                          />

                         </View>
                      </ImageBackground>
                 

            
         </View>
         <View style={{
          paddingLeft:10,
          height:deviceWidth/4,width:deviceWidth/2.5,
          marginBottom:10,
          marginLeft:5,
          borderRadius: 2,
          marginTop:'2%'
         }}>
             


                      <ImageBackground                     
                       source={require("../../../../assets/images/image_1692.png")}
                       style={{ height:deviceWidth/4,width:deviceWidth/2.5,resizeMode:'contain'}}
                      >

                       <View style={{position:'absolute',top:5,left:5}}>
                         <Image                      
                           source={require("../../../../assets/images/Frame_137.png")}
                           style={{ width:20, height: 20,padding:10,resizeMode:'contain'}}
                          />

                         </View>
                      </ImageBackground>
                 

            
         </View>
         <View style={{
          paddingLeft:10,
          height:deviceWidth/4,width:deviceWidth/2.5,
          marginBottom:10,
          marginLeft:5,
          borderRadius: 2,
          marginTop:'2%'
         }}>
             


                      <ImageBackground                     
                       source={require("../../../../assets/images/image_1692.png")}
                       style={{ height:deviceWidth/4,width:deviceWidth/2.5,resizeMode:'contain'}}
                      >

                       <View style={{position:'absolute',top:5,left:5}}>
                         <Image                      
                           source={require("../../../../assets/images/Frame_137.png")}
                           style={{ width:20, height: 20,padding:10,resizeMode:'contain'}}
                          />

                         </View>
                      </ImageBackground>
                 

            
         </View>

            </ScrollView>
            <ScrollView horizontal={true} style={{marginLeft:-5}}>

             
         <View style={{
          paddingLeft:10,
          height:deviceWidth/4,width:deviceWidth/2.4,
          marginBottom:10,
          
          borderRadius: 2,
          marginTop:'2%'
         }}>
             


                      <ImageBackground                     
                       source={require("../../../../assets/images/image_1695.png")}
                       style={{ height:deviceWidth/4,width:deviceWidth/2.5,resizeMode:'contain'}}
                      >

                       <View style={{position:'absolute',top:5,left:5}}>
                         <Image                      
                           source={require("../../../../assets/images/Frame_137.png")}
                           style={{ width:20, height: 20,padding:10,resizeMode:'contain'}}
                          />

                         </View>
                      </ImageBackground>
                 

            
         </View>
         <View style={{
          paddingLeft:10,
          height:deviceWidth/4,width:deviceWidth/2.4,
          marginBottom:10,
          marginLeft:5,
          borderRadius: 2,
          marginTop:'2%'
         }}>
             


                      <ImageBackground                     
                       source={require("../../../../assets/images/image_1694.png")}
                       style={{ height:deviceWidth/4,width:deviceWidth/2.5,resizeMode:'contain'}}
                      >

                       <View style={{position:'absolute',top:5,left:5}}>
                         <Image                      
                           source={require("../../../../assets/images/Frame_137.png")}
                           style={{ width:20, height: 20,padding:10,resizeMode:'contain'}}
                          />

                         </View>
                      </ImageBackground>
                 

            
         </View>
         <View style={{
          paddingLeft:10,
          height:deviceWidth/4,width:deviceWidth/2.8,
          marginBottom:10,
          marginLeft:5,
          borderRadius: 2,
          marginTop:'2%'
         }}>
             


                      <ImageBackground                     
                       source={require("../../../../assets/images/image_1692.png")}
                       style={{ height:deviceWidth/4,width:deviceWidth/2.5,resizeMode:'contain'}}
                      >

                       <View style={{position:'absolute',top:5,left:5}}>
                         <Image                      
                           source={require("../../../../assets/images/Frame_137.png")}
                           style={{ width:20, height: 20,padding:10,resizeMode:'contain'}}
                          />

                         </View>
                      </ImageBackground>
                 

            
         </View>

            </ScrollView>
          </View>
           

           
          </View>

         </View>


         <TouchableOpacity style={{marginHorizontal:'5%',borderRadius:20,backgroundColor:'#D7E0E7',justifuContent:'center',alignItems:'center',padding:'3%',marginBottom:'5%'}} onPress={()=>Actions.New3()}>
             <Text style={{color:'#fff'}}>Далее</Text>
         </TouchableOpacity> 
         </ScrollView>      
      </Container>
    );
  }
}



New2 = reduxForm({
  
})(New2);

New2 = connect(mapStateToProps)(New2);

export default New2;
