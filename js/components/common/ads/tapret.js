import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Platform, View,Dimensions,Image,TouchableOpacity,StatusBar,ImageBackground,StyleSheet ,FlatList,TextInput,CheckBox} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Spinner,
  Body,
  Toast,
  Input,

} from "native-base";
import { Actions } from "react-native-router-flux";
import * as appStateSelector from "../../../reducers/driver/appState";
//import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import AsyncStorage from '@react-native-community/async-storage';
import pr from 'pr-unit';
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import { checkUser, userLoginRequest,getremaningtimer,setlistorder,setgenderchoice } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";
import RNPickerSelect from 'react-native-picker-select';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import _ from "lodash";
import { changePageStatus, currentLocationUser, signInUser } from '../../../actions/driver/home';
import { fetchUserCurrentLocationAsync, syncDataAsync, mapDeviceIdToUser,getpooldata } from '../../../actions/driver/home';
import OneSignal from "react-native-onesignal";
import config from "../../../../config";
import Modal from "react-native-modal";
import Contacts from 'react-native-contacts';

//import DeviceInfo from 'react-native-device-info';
import { emit } from "../../../services/socket";
let that = null;
const deviceWidth = Dimensions.get('window').width; 
const deviceHeight = Dimensions.get('window').height; 




function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  console.log('state.driver.appState',state.app);
  return {
   
  };
}

class Tapret extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    
    
  };
 timer = null;
  

  constructor(props) {
    super(props);
    

    this.state = {
      selected:false,
      selected2:false
    };
    
  }

 

  componentWillUnmount(){
   
  }
 
 setSelection(){
  if(this.state.selected==true){
    this.setState({
      selected:false
    })
  } else{
    this.setState({
      selected:true
    })
  }
 }
 setSelection2(){
  if(this.state.selected2==true){
    this.setState({
      selected2:false
    })
  } else if(this.state.selected2==false){
    this.setState({
      selected2:true
    })
  }
 }
  render() {
   
    
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <StatusBar
            animated={true}
            backgroundColor='rgba(205, 178, 108, 0.1)'
            barStyle="dark-content"
            showHideTransition="none"
          />
               <View style={styles.mainmenu2}>
                <TouchableOpacity onPress={()=>Actions.pop()}>
                   <Image                      
                       source={require("../../../../assets/images/Stroke.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                <View style={{justifuContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:17,color:'black'}}>Таргет</Text>
                </View>
                <TouchableOpacity>
                   <Image                      
                       source={require("../../../../assets/images/XCircle.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                   
            </View>
            <View style={{flex:1}}>
       <View style={styles.text}>
          <View style={{backgroundColor:'#5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
          <View style={{backgroundColor:'#5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>  
       </View>
      
         <View style={{backgroundColor:'#FBFBFD',borderRadius:20,marginHorizontal:20,marginTop:10}}>
                       <Image                      
                        source={require("../../../../assets/images/Search.png")}
                        style={{ width:14, height: 14,resizeMode:'contain',position:'absolute',left:10,top:15}}
                      />
                       <TextInput
                          placeholder='Введи название стран для запуска'
                          style={{ borderColor:'#e6e6e6',  color:'#000', width:deviceWidth-20, fontSize:13,width:'95%',paddingLeft:'10%'}}
                          multiline
                          numberOfLines={1}
                          placeholderTextColor="#777"
                          editable={true}
                           onChange={(event) => this.setState({description: event.nativeEvent.text})}
                           value={this.state.description}
                          
                        />  
         </View>
         <View style={{elevation: 5,shadowColor: '#000',margin:'3%',padding:'3%',height:deviceHeight/4.5,borderRadius:15,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
    backgroundColor:'#fff',}}>
             <Text style={{fontSize:16,padding:'5%',color:'#1C1C1C'}}>Список стран</Text>

             <View style={{elevation: 5,shadowColor: '#000',margin:'3%',padding:'3%',borderRadius:5,width:deviceWidth/5,
                  shadowOffset: { width: 0, height: 1 },
                  shadowOpacity: 0.8,
                  shadowRadius: 1,
                  backgroundColor:'#fff'}}>
                  <Image                      
                        source={require("../../../../assets/images/Shape.png")}
                        style={{ width:14, height: 14,resizeMode:'contain',position:'absolute',left:-5,top:-10}}
                      />
                <Text style={{fontSize:14,color:'#172026'}}>Украина</Text>
             </View>
         </View>
         <View style={{elevation: 5,shadowColor: '#000',margin:'3%',padding:'3%',height:deviceHeight/3.5,borderRadius:15,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
    backgroundColor:'#fff',}}>
             <Text style={{fontSize:16,padding:'5%',color:'#1C1C1C'}}>Аудитория</Text>
             <View style={{flexDirection:'row',marginHorizontal:20}}>
                  <View style={{borderWidth:2,borderColor:'#e6e6e6',width:deviceWidth/5}}></View>
                  <View style={{borderWidth:5,borderColor:'#5B63F4',width:deviceWidth/20,borderRadius:10,height:20,position:'absolute',left:'20%',zIndex:1001,top:-6}}></View>
                  <View style={{borderWidth:2,borderColor:'#5B63F4',width:deviceWidth/2.6}}></View>
                  <View style={{borderWidth:5,borderColor:'#5B63F4',width:deviceWidth/20,borderRadius:10,height:20,position:'absolute',right:'20%',zIndex:1001,top:-6}}></View>
                  <View style={{borderWidth:2,borderColor:'#e6e6e6',width:deviceWidth/5}}></View>
            </View> 
            <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:10}}>
             <View style={styles.checkboxContainer}>
              <CheckBox
                value={this.state.selected2}
                onValueChange={()=>this.setSelection2()}
                style={styles.checkbox}
              />
              <Text style={styles.label}>Мужчины</Text>
             </View>
             <View style={styles.checkboxContainer}>
              <CheckBox
                value={this.state.selected}
                onValueChange={()=>this.setSelection()}
                style={styles.checkbox}
              />
              <Text style={styles.label}>Женщины</Text>
             </View>
             </View>
         </View>
        
         
         </View>

         <TouchableOpacity style={{marginHorizontal:'5%',borderRadius:20,backgroundColor:'#5B63F4',justifuContent:'center',alignItems:'center',padding:'3%',marginBottom:'5%'}} onPress={()=>Actions.Interest()}>
             <Text style={{color:'#fff'}}>Далее</Text>
         </TouchableOpacity>       
      </Container>
    );
  }
}



Tapret = reduxForm({
  
})(Tapret);

Tapret = connect(mapStateToProps)(Tapret);

export default Tapret;
