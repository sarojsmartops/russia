import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Platform, View,Dimensions,Image,TouchableOpacity,StatusBar,ImageBackground,StyleSheet ,FlatList,TextInput,ScrollView} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Spinner,
  Body,
  Toast,
  Input,

} from "native-base";
import { Actions } from "react-native-router-flux";
import * as appStateSelector from "../../../reducers/driver/appState";
//import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import AsyncStorage from '@react-native-community/async-storage';
import pr from 'pr-unit';
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import { checkUser, userLoginRequest,getremaningtimer,setlistorder,setgenderchoice } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";
import RNPickerSelect from 'react-native-picker-select';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import _ from "lodash";
import { changePageStatus, currentLocationUser, signInUser } from '../../../actions/driver/home';
import { fetchUserCurrentLocationAsync, syncDataAsync, mapDeviceIdToUser,getpooldata } from '../../../actions/driver/home';
import OneSignal from "react-native-onesignal";
import config from "../../../../config";
import Modal from "react-native-modal";
import Contacts from 'react-native-contacts';

//import DeviceInfo from 'react-native-device-info';
import { emit } from "../../../services/socket";
let that = null;
const deviceWidth = Dimensions.get('window').width; 
const deviceHeight = Dimensions.get('window').height; 




function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  console.log('state.driver.appState',state.app);
  return {
   
  };
}

class Last extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    
    
  };
 timer = null;
  

  constructor(props) {
    super(props);
    

    this.state = {
     
    };
    
  }

 

  componentWillUnmount(){
   
  }
 
 
  render() {
   
    
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <StatusBar
            animated={true}
            backgroundColor='rgba(205, 178, 108, 0.1)'
            barStyle="dark-content"
            showHideTransition="none"
          />
          <View style={{flex:1,}}>
              <View style={{justifyContent:'center',alignItems:'center',marginTop:deviceHeight/4.5}}>
              <View style={{width:40,height:40,borderRadius:20,backgroundColor:'#5B63F4',justifyContent:'center',alignItems:'center'}}>
                 <Image                      
                       source={require("../../../../assets/images/Stopwatch.png")}
                      style={{ width:30, height: 30,padding:10,}}
                      />

                </View>
                 <Text style={{fontSize:15,color:'#000',fontWeight:'bold'}}>Отлично</Text>
                 <View style={{marginHorizontal:'10%',justifyContent:'center',alignItems:'center',}}>
                 <Text style={{color:'#000',textAlign:'center'}}>Ты успешно запустил свою первую рекламную кампанию</Text>
                 </View>
              </View>
              <View style={{backgroundColor:'#F6F7FB',marginHorizontal:'20%',borderRadius:10,marginTop:7,padding:'1%'}}>
                <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'center'}}>
                 <Text style={{marginLeft:10,fontSize:8}}>Подключаем CRM-систему</Text>
                 
                 <Image                      
                       source={require("../../../../assets/images/Check_crfr.png")}
                      style={{ width:12, height: 12,}}
                      />
                  </View>
              </View>
              <View style={{backgroundColor:'#F6F7FB',marginHorizontal:'15%',borderRadius:20,marginTop:'3%',flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',}}>
              
                 <Text style={{fontSize:12,padding:'3%',marginTop:5}}>Добавляем рекламные платформы</Text>
                
                 <Image                      
                       source={require("../../../../assets/images/Check_crfr.png")}
                      style={{ width:18, height: 18,}}
                      />
                  
                  
              </View>
              <View style={{backgroundColor:'#5B63F4',marginHorizontal:'10%',borderRadius:25,marginTop:'3%',flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',}}>
              
                 <Text style={{fontSize:12,color:'#fff',padding:'3%',marginTop:5}}>Запускаем аналитику </Text>
                
                 <Image                      
                       source={require("../../../../assets/images/Check.png")}
                      style={{ width:18, height: 18,}}
                      />
                  
                  
              </View>
              <View style={{backgroundColor:'#F6F7FB',marginHorizontal:'15%',borderRadius:20,marginTop:'3%',flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',}}>
              
                 <Text style={{fontSize:12,padding:'3%',marginTop:5}}>Активируем алгоритм</Text>
                
                 
                  
                  
              </View>
              <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',backgroundColor:'#F6F7FB',marginHorizontal:'20%',borderRadius:10,marginTop:7,padding:'1%'}}>
                 <Text style={{marginLeft:10,fontSize:8}}>Подключаем CRM-систему</Text>
                 
                 
                  </View>
              <View style={{justifyContent:'center',alignItems:'center',marginTop:'10%'}}>
               <Text style={{color:'#000',fontWeight:'bold',fontSize:15}}>Ожидайте модерации</Text>
               <Text style={{fontSize:15}}>Вы получите уведомление в личку</Text>

           </View>
          </View>
           

         <TouchableOpacity style={{marginHorizontal:'5%',borderRadius:20,backgroundColor:'#5B63F4',justifuContent:'center',alignItems:'center',padding:'3%',marginBottom:'5%'}}>
             <Text style={{color:'#fff'}}>Готово</Text>
             
         </TouchableOpacity>       
      </Container>
    );
  }
}



Last = reduxForm({
  
})(Last);

Last = connect(mapStateToProps)(Last);

export default Last;
