import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Platform, View,Dimensions,Image,TouchableOpacity,StatusBar,ImageBackground,StyleSheet ,FlatList,TextInput,CheckBox} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Spinner,
  Body,
  Toast,
  Input,

} from "native-base";
import { Actions } from "react-native-router-flux";
import * as appStateSelector from "../../../reducers/driver/appState";
//import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import AsyncStorage from '@react-native-community/async-storage';
import pr from 'pr-unit';
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import { checkUser, userLoginRequest,getremaningtimer,setlistorder,setgenderchoice } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";
import RNPickerSelect from 'react-native-picker-select';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import _ from "lodash";
import { changePageStatus, currentLocationUser, signInUser } from '../../../actions/driver/home';
import { fetchUserCurrentLocationAsync, syncDataAsync, mapDeviceIdToUser,getpooldata } from '../../../actions/driver/home';
import OneSignal from "react-native-onesignal";
import config from "../../../../config";
import Modal from "react-native-modal";
import Contacts from 'react-native-contacts';

//import DeviceInfo from 'react-native-device-info';
import { emit } from "../../../services/socket";
let that = null;
const deviceWidth = Dimensions.get('window').width; 
const deviceHeight = Dimensions.get('window').height; 




function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  console.log('state.driver.appState',state.app);
  return {
   
  };
}

class Ads2 extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    
    
  };
 timer = null;
  

  constructor(props) {
    super(props);
    

    this.state = {
      selected:false,
      selected2:false,
      selected3:false,
      selected4:false,
      redio:true,
    };
    
  }

 setSelection(){
  if(this.state.selected==true){
    this.setState({
      selected:false
    })
  } else{
    this.setState({
      selected:true
    })
  }
 }
 setSelection2(){
  if(this.state.selected2==true){
    this.setState({
      selected2:false
    })
  } else if(this.state.selected2==false){
    this.setState({
      selected2:true
    })
  }
 }
setSelection3(){
  if(this.state.selected3==true){
    this.setState({
      selected3:false
    })
  } else{
    this.setState({
      selected:true
    })
  }
 }
 setSelection4(){
  if(this.state.selected4==true){
    this.setState({
      selected4:false
    })
  } else if(this.state.selected4==false){
    this.setState({
      selected4:true
    })
  }
 }
  componentWillUnmount(){
   
  }
 radiofun(value){
  if(this.state.redio==false){
    this.setState({
      redio:true
    })
  }else{
    this.setState({
      redio:false
    })
  }
 }
 
  render() {
   
    
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <StatusBar
            animated={true}
            backgroundColor='rgba(205, 178, 108, 0.1)'
            barStyle="dark-content"
            showHideTransition="none"
          />
               <View style={styles.mainmenu2}>
                <TouchableOpacity onPress={()=>Actions.pop()}>
                   <Image                      
                       source={require("../../../../assets/images/Stroke.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                <View style={{justifuContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:17,color:'black',fontWeigth:'bold'}}>Рекламная кампания</Text>
                </View>
                <TouchableOpacity>
                   <Image                      
                       source={require("../../../../assets/images/XCircle.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                   
            </View>
            <View style={{flex:1}}>
       <View style={styles.text}>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>  
       </View>
       <View style={{marginHorizontal:'10%',marginTop:'10%'}}>
       <View style={{}}>
          <Text style={{fontSize:13,color:'black'}}>Название рекламной компании</Text>
       </View>

        
                        <TextInput
                          placeholder='Название'
                          style={{ borderColor:'#e6e6e6', borderBottomWidth:1,  color:'#000', width:deviceWidth-20, fontSize:13,width:'95%'}}
                          multiline
                          numberOfLines={1}
                          placeholderTextColor="#777"
                          editable={true}
                           onChange={(event) => this.setState({description: event.nativeEvent.text})}
                           value={this.state.description}
                          
                        />            
         </View> 
         <View style={{marginHorizontal:'10%',marginTop:'10%'}}>
       <View style={{}}>
          <Text style={{fontSize:15,color:'#172026',fontWeigth:'bold'}}>Выберите сайт</Text>
          <Text style={{fontSize:13,color:'#172026'}}>Каких результатов ты хочешь добиться с помощью рекламы? </Text>
       </View>
            
             <View style={styles.checkboxContainer2}>
             <TouchableOpacity onPress={()=>this.radiofun(1)}>
             {this.state.redio ?
                  <Image                      
                      source={require("../../../../assets/images/radio_empty.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      
                      />
                      :
                      <Image                      
                       source={require("../../../../assets/images/radio_filling.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                    }
                    </TouchableOpacity>
              <Text style={styles.label}>mango.qwik.ws</Text>
             </View>
             
             
             <View style={styles.checkboxContainer2}>
              <TouchableOpacity onPress={()=>this.radiofun(2)}>
             {this.state.redio ?
                  <Image                      
                      source={require("../../../../assets/images/radio_empty.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      
                      />
                      :
                      <Image                      
                       source={require("../../../../assets/images/radio_filling.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                    }
                    </TouchableOpacity>
              <Text style={styles.label}>lime.qwik.ws</Text>
             </View>
             <View style={styles.checkboxContainer2}>
              <TouchableOpacity onPress={()=>this.radiofun(3)}>
             {this.state.redio ?
                  <Image                      
                      source={require("../../../../assets/images/radio_empty.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      
                      />
                      :
                      <Image                      
                       source={require("../../../../assets/images/radio_filling.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                    }
                    </TouchableOpacity>
              <Text style={styles.label}>apple.qwik.ws</Text>
             </View>
             <View style={styles.checkboxContainer2}>
              <TouchableOpacity onPress={()=>this.radiofun(4)}>
             {this.state.redio ?
                  <Image                      
                      source={require("../../../../assets/images/radio_empty.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      
                      />
                      :
                      <Image                      
                       source={require("../../../../assets/images/radio_filling.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                    }
                    </TouchableOpacity>
              <Text style={styles.label}>Добавить сторонний сайт</Text>
             </View>
       
       
       
        
                                 
         </View>  
         
         </View>
         <TouchableOpacity style={{marginHorizontal:'5%',borderRadius:20,backgroundColor:'#5B63F4',justifuContent:'center',alignItems:'center',padding:'3%',marginBottom:'5%'}} onPress={()=>Actions.Ads3()}>
             <Text style={{color:'#fff'}}>Далее</Text>
         </TouchableOpacity>       
      </Container>
    );
  }
}



Ads2 = reduxForm({
  
})(Ads2);

Ads2 = connect(mapStateToProps)(Ads2);

export default Ads2;
