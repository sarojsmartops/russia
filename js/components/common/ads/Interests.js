import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Platform, View,Dimensions,Image,TouchableOpacity,StatusBar,ImageBackground,ScrollView,StyleSheet ,FlatList,TextInput,CheckBox} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Spinner,
  Body,
  Toast,
  Input,

} from "native-base";
import { Actions } from "react-native-router-flux";
import * as appStateSelector from "../../../reducers/driver/appState";
//import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import AsyncStorage from '@react-native-community/async-storage';
import pr from 'pr-unit';
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import { checkUser, userLoginRequest,getremaningtimer,setlistorder,setgenderchoice } from "../../../actions/common/checkUser";
import ModalView from "../ModalView";
import RNPickerSelect from 'react-native-picker-select';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import _ from "lodash";
import { changePageStatus, currentLocationUser, signInUser } from '../../../actions/driver/home';
import { fetchUserCurrentLocationAsync, syncDataAsync, mapDeviceIdToUser,getpooldata } from '../../../actions/driver/home';
import OneSignal from "react-native-onesignal";
import config from "../../../../config";
import Modal from "react-native-modal";
import Contacts from 'react-native-contacts';

//import DeviceInfo from 'react-native-device-info';
import { emit } from "../../../services/socket";
let that = null;
const deviceWidth = Dimensions.get('window').width; 
const deviceHeight = Dimensions.get('window').height; 




function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  console.log('state.driver.appState',state.app);
  return {
   
  };
}

class Interest extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    
    
  };
 timer = null;
  

  constructor(props) {
    super(props);
    

    this.state = {
      selected:false
    };
    
  }

 

  componentWillUnmount(){
   
  }
 
 setSelection(){
  if(this.state.selected==true){
    this.setState({
      selected:false
    })
  } else{
    this.setState({
      selected:true
    })
  }
 }
  render() {
   
    
    return (
      <Container style={{ backgroundColor: "#fff" }}>
      <StatusBar
            animated={true}
            backgroundColor='rgba(205, 178, 108, 0.1)'
            barStyle="dark-content"
            showHideTransition="none"
          />
               <View style={styles.mainmenu2}>
                <TouchableOpacity onPress={()=>Actions.pop()}>
                   <Image                      
                       source={require("../../../../assets/images/Stroke.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                <View style={{justifuContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:17,color:'black'}}>Интересы</Text>
                </View>
                <TouchableOpacity>
                   <Image                      
                       source={require("../../../../assets/images/XCircle.png")}
                      style={{ width:14, height: 7,padding:10,resizeMode:'contain'}}
                      />
                  </TouchableOpacity>
                   
            </View>
            <View style={{flex:1}}>
       <View style={styles.text}>
          <View style={{backgroundColor:'#5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
          <View style={{backgroundColor:'#5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
          <View style={{backgroundColor:'5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View> 
          <View style={{backgroundColor:'5B63F4',borderWidth:1,borderColor:'#5B63F4',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View> 
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>
          <View style={{backgroundColor:'gray',borderWidth:1,borderColor:'#e6e6e6',width:'10%'}}></View>  
       </View>
      <ScrollView>
         <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026',fontWeight:'bold'}}>Выберите интересы</Text>
            <Text style={{fontSize:13,color:'#172026'}}>Каких результатов вы хотите добиться с помощью этой продвигаемой публикации?</Text>
            <Text style={{fontSize:13,color:'#172026',marginTop:10}}>Сэкономьте <Text style={{color:'blue'}}>(usefulness) </Text>15% <Text style={{color:'blue'}}>(ultra specificity)</Text> стоимости квартиры в январе <Text style={{color:'blue'}}>(urgency)</Text> за счет покупки на этапе строительства <Text style={{color:'blue'}}>(uniqueness)</Text></Text>
         </View>
       
         <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026'}}>Семья и отношения</Text>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                <View>
                    <Text>💍 Брак</Text>
                  </View>
                  <View>
                      <Text>🚸 Воспитание детей</Text>
                    </View>
                     <Text>🤰 Материнство</Text>
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                <Text>💒 Свадьбы</Text>
                <Text>💘 Свидания</Text>
                <Text>👪 Семья</Text>
             </View>
         </View>
         <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026'}}>Образование и обучение </Text>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                <View style={{backgroundColor:'#5B63F4',padding:'2%',}}>
                 
                <Text style={{color:'#fff'}}>🏫 Школа</Text>
                  </View>
                  <View>
                      <Text>🎓 Университет</Text>
                    </View>
                      <Text>📜 Курс обучения </Text>
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                      <Text>📙 Высшее образование</Text>
                      <Text>📗 Среднее образование</Text>
                      
             </View>
         </View>
         <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026'}}>Online</Text>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                <View style={{backgroundColor:'#5B63F4',padding:'2%',}}>
                    <Text style={{color:'#fff'}}>💻 Digital-маркетинг</Text>
                  </View>
                  <View>
                      <Text>📱 Маркетинг в социальных сетях</Text>
                  </View>
                      
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                      <Text>🎁 Оптимизация в поисковиках</Text>
                      <Text>🖥 Веб-разработка</Text>
                      
             </View>
         </View>
          <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026'}}>Бизнес и индустрия </Text>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                  <View style={{padding:'2%',}}>
                    <Text>🏛 Архитектура</Text>
                  </View>
                  <View>
                      <Text>💳 Банковские услуги</Text>
                    </View>
                      
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
             <View style={{backgroundColor:'#5B63F4',padding:'2%'}}>
                 <Text style={{color:'#fff'}}>🔬 Наука</Text>
                </View>
                     <Text>📋 Менеджмент</Text>
                     <Text>🏘 Недвижимость</Text>
                      
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10,marginTop:15}}>
                <View style={{padding:'2%',backgroundColor:'#5B63F4'}}>
                  <Text style={{color:'#fff'}}>🏗 Строительство</Text>
                  </View>
                  <View>
                      <Text>👩‍👦 Уход и сиделки</Text>
                    </View>
                      
             </View>
         </View>
          <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026'}}>Спорт и фитнес </Text>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10,marginTop:15}}>
                <View style={{flexDirection:'row'}}>
                 
                      <Text>🏅 Спорт</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <Image                      
                       source={require("../../../../assets/images/image_1687.png")}
                      style={{ width:25, height: 25,padding:10,resizeMode:'contain'}}
                      />
                      <Text>Физические упражнения</Text>
                    </View>
                      
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:15}}>
             <View style={{flexDirection:'row'}}>
                   <Image                      
                       source={require("../../../../assets/images/image_1685.png")}
                      style={{ width:25, height: 25,padding:10,resizeMode:'contain'}}
                      />
                      <Text>Йога</Text>
              </View>
              <View style={{flexDirection:'row'}}>
                      <Image                      
                       source={require("../../../../assets/images/image_1686.png")}
                      style={{ width:25, height: 25,padding:10,resizeMode:'contain'}}
                      />
                      <Text>Бег</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <Image                      
                       source={require("../../../../assets/images/image_1696.png")}
                      style={{ width:25, height: 25,padding:10,resizeMode:'contain'}}
                      />
                      <Text>Медитация</Text>
                  </View>
                      
             </View>
         </View>
         <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026'}}>Занятия на свежем воздухе </Text>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10,marginTop:15}}>
                <View style={{flexDirection:'row'}}>
                 
                      <Text>🏇🏽 Верховая езда</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      
                      <Text>🛶 Лодочный спорт</Text>
                    </View>
                      
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:15}}>
             <View style={{flexDirection:'row'}}>
                   
                      <Text>🏕 Походы на природу</Text>
              </View>
              <View style={{flexDirection:'row'}}>
                      
                      <Text>🦈 Рыбалка</Text>
                  </View>
                 
                      
             </View>
         </View>
         <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026'}}>Покупки и мода </Text>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10,marginTop:15}}>
                <View style={{flexDirection:'row'}}>
                      
                      <Image                      
                       source={require("../../../../assets/images/image_1688.png")}
                      style={{ width:25, height: 25,padding:10,resizeMode:'contain'}}
                      />
                      <Text> Игрушки</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      
                      <Text>🎧 Аксессуары</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                      
                      <Text>👕 Одежда</Text>
                    </View>
                      
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:15}}>
                     <View style={{flexDirection:'row'}}>
                      
                      <Text>🛍 Покупки</Text>
                  </View>
                  
                      
             </View>
         </View>
         <View style={{marginHorizontal:10,marginVertical:10,padding:'4%'}}>
            <Text style={{fontSize:15,color:'#172026'}}>Покупки и мода </Text>
             <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10,marginTop:15}}>
                <View style={{flexDirection:'row'}}>
                      
                      <Image                      
                       source={require("../../../../assets/images/image_1688.png")}
                      style={{ width:25, height: 25,padding:10,resizeMode:'contain'}}
                      />
                      <Text> Игрушки</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      
                      <Text>🎧 Аксессуары</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                      
                      <Text>👕 Одежда</Text>
                    </View>
                      
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:15}}>
                     <View style={{flexDirection:'row'}}>
                      
                      <Text>🛍 Покупки</Text>
                  </View>
                  
                      
             </View>
         </View>
         

         </ScrollView>
        
         
         </View>

         <TouchableOpacity style={{marginHorizontal:'5%',borderRadius:20,backgroundColor:'#5B63F4',justifuContent:'center',alignItems:'center',padding:'3%',marginBottom:'5%'}} onPress={()=>Actions.New()}>
             <Text style={{color:'#fff'}}>Далее</Text>
         </TouchableOpacity>       
      </Container>
    );
  }
}



Interest = reduxForm({
  
})(Interest);

Interest = connect(mapStateToProps)(Interest);

export default Interest;
